import React, { useEffect, useState } from "react";
import { EventsEmit, EventsOff, EventsOn, WindowGetSize } from "../wailsjs/runtime";
import { SimpleTreeView, TreeItem } from "@mui/x-tree-view";
import MDEditor from "@uiw/react-md-editor";
import rehypeRewrite, { getCodeString } from "rehype-rewrite";
import OBInputPopup from "./Input";

const REG = /\B(\#[a-zA-Z]+\b)(?!;)/gi;
const baseurl = "wails://wails.localhost:34115/";

export interface ITreeDetails {
  Id: string;
  Label: string;
  IsDir: boolean;
  Children: ITreeDetails[];
}

function App() {
  const [value, setValue] = React.useState("");
  const [tree, setTree] = useState([]);
  const [selectedFile, setSelectedFile] = useState("");
  const [selectionIsDir, setSelectionIsDir] = useState(false);
  const [markForDelete, setMarkForDelete] = useState(false);
  const [markForSave, setMarkForSave] = useState(false);

  const [open, setOpen] = React.useState(false);
  const [openNewNote, setOpenNewNote] = React.useState(false);
  const [openNewBook, setOpenNewBook] = React.useState(false);
  const [openMove, setOpenMove] = React.useState(false);

  const [winHeight, setWinHeight] = React.useState(500);
  const [defaultFileName, setDefaultFileName] = React.useState("");

  const handleClose = () => {
    setOpen(false);
    setOpenNewNote(false);
    setOpenNewBook(false);
    setOpenMove(false);
  };

  const styles = {
    mainRow: {
      height: "100%",
      width: "100%",
      display: "flex",
      flexDirection: "row",
    },
    rightCol: {
      height: "100%",
      width: "100%",
      display: "flex",
      flexDirection: "column",
    },
  } as const;

  useEffect(() => {
    EventsOn("load_tree", setTree);
    EventsOn("read_file", setValue);
    EventsOn("open_file", setValue);
    EventsOn("new_file", handlePromptForNewNoteName);
    EventsOn("create_book", createBook);
    EventsOn("delete_file", deleteSelectedNote);
    EventsOn("save_note", saveNote);
    EventsOn("rename_file", renameNote);
    EventsOn("move", move);
    EventsEmit("web_loaded");

    document.addEventListener("click", onClickHandler);


    return () => {
      EventsOff("load_tree");
      EventsOff("read_file");
      EventsOff("open_file");
      EventsOff("new_file");
      EventsOff("create_book");
      EventsOff("delete_file");
      EventsOff("save_note");
      EventsOff("rename_file");
      EventsOff("move");
      EventsOff("web_loaded");
      EventsOff("click");
    };
  }, []);

  useEffect(() => {
    if (selectedFile && markForDelete) {
      if (selectionIsDir) {
        EventsEmit("delete_book", selectedFile);
      } else {
        EventsEmit("delete_note", selectedFile);
      }

      setSelectedFile("");
      setMarkForDelete(false);
    }
  }, [markForDelete]);

  useEffect(() => {
    if (selectedFile && markForSave) {
      EventsEmit("save_file", selectedFile, value);
      setMarkForSave(false);
    }
  }, [markForSave]);

  const onClickHandler = (e: MouseEvent) => {
    const input = e.target as HTMLLinkElement;
    if (input.href != null) {
      e.preventDefault();
      let p = input.href
        .substring(baseurl.length - 1, input.href.length)
        .split("/");
      EventsEmit("load_file", p);
    }
  };

  const handlePromptForNewNoteName = () => {
    setDefaultFileName(getDefaultName)
    setOpenNewNote(true);
  };

  const deleteSelectedNote = () => {
    setMarkForDelete(true);
  };

  const saveNote = () => {
    setMarkForSave(true);
  };

  const renameNote = () => {
    setOpen(true);
  };

  const createBook = () => {
    setOpenNewBook(true);
  };

  const move = () => {
    setOpenMove(true);
  };

  const getDefaultName = () => {
    let d = new Date().toISOString().split("T");
    return d[0] + "_" + d[1].split(".")[0].replace(/:/g, "-");
  }

  const buildTree = (t: ITreeDetails[]) => {
    console.log(t)

    return t?.map((d: ITreeDetails, i: number) => (
      <TreeItem
        onClick={() => {
          setSelectionIsDir(d.IsDir);
          setSelectedFile(d.Id);
          if (!d.IsDir) {
            EventsEmit("load_file", d.Id);
          }
        }}
        key={d.Id}
        itemId={d.Label}
        label={d.Label}
      >
        {buildTree(d.Children)}
      </TreeItem>
    ));
  };

  WindowGetSize().then((size) => {
    console.log(size)
    setWinHeight(size.h)
  })

  return (
    <div style={styles.rightCol}>
      <div style={styles.mainRow}>
        <SimpleTreeView
          aria-label="file system navigator"
          sx={{ flexGrow: 1, minWidth: 200, overflowY: "auto" }}
        >
          {buildTree(tree)}
        </SimpleTreeView>
        <div style={styles.rightCol}>
          <MDEditor
            style={{ width: "100%", height: "100%" }}
            value={value}
            height={winHeight}
            hideToolbar={true}
            onChange={(event) => {
              setValue(event ?? "");
            }}
            previewOptions={{
              rehypePlugins: [
                [
                  rehypeRewrite,
                  {
                    rewrite: (node: any, index: any, parent: any) => {
                      if (node.type === "element" && node.tagName != "a") {
                        let text = getCodeString(node.children);
                        let m;
                        let parts: any[] = [];
                        if (REG.test(text)) {
                          m = text.split(" ") 

                          for (let element of m) {
                            if (REG.test(element)) {
                              parts.push(
                                {
                                  type: "element",
                                  tagName: "a",
                                  properties: {
                                    href: "https://google.ca",
                                  },
                                  children: [{ type: "text", value: " " + element + " " }],
                                })
                            } else {
                              parts.push(
                                {
                                  type: 'element',
                                  tagName: 'span',
                                  properties: {},
                                  children: [
                                    { type: 'text', value: element }
                                  ]
                                })
                            }
                          }
                        }
                        console.log(parts)
                        if (parts.length > 0) {
                          node.children = parts;
                        }
                      }
                    },
                  },
                ],
              ],
            }}
          />
        </div>
      </div>

      <OBInputPopup
        title="Rename"
        content="Rename"
        label="New name"
        open={open}
        handleClose={handleClose}
        dispatcher={(p: string) => {
          let parts = selectedFile.split("/");
          parts.pop();
          if (selectionIsDir) {
            EventsEmit("rename_book", selectedFile, parts.join("/") + "/" + p);
          } else {
            EventsEmit(
              "rename_note",
              selectedFile,
              parts.join("/") + "/" + p + ".md"
            );
          }
        }}
      />

      <OBInputPopup
        title="New"
        content="Note Name"
        label="New note name"
        value={defaultFileName}
        open={openNewNote}
        handleClose={handleClose}
        dispatcher={(p: string) => {
          p = selectedFile + "/" + p;
          if (selectionIsDir) {
            EventsEmit("new_note", p);
          } else {
            let parts = p.split("/");
            parts.pop();
            EventsEmit("new_note", parts.join("/"), p);
          }
        }}
      />

      <OBInputPopup
        title="New"
        content="Book Name"
        label="New book name"
        open={openNewBook}
        handleClose={handleClose}
        dispatcher={(p: string) => {
          EventsEmit("new_book", selectedFile + "/" + p);
        }}
      />

      <OBInputPopup
        title="Move"
        content="Book Path"
        label="Move to book path"
        open={openMove}
        handleClose={handleClose}
        dispatcher={(p: string) => {
          let src = selectedFile;
          let dest = p + "/" + selectedFile.split("/").pop();
          if (selectionIsDir) {
            EventsEmit("move_book", src, dest);
          } else {
            EventsEmit("move_note", src, dest);
          }
        }}
      />
    </div>
  );
}

export default App;
