import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
} from "@mui/material";
import { ReactElement } from "react";

type OBInputPopupProps = {
  title: string;
  content: string;
  label: string;
  open: boolean;
  value?: string;
  handleClose: () => void;
  dispatcher: (path: string) => void;
};

const OBInputPopup = (props: OBInputPopupProps): ReactElement => {
  return (
    <Dialog
      disableRestoreFocus 
      open={props.open}
      onClose={props.handleClose}
      PaperProps={{
        component: "form",
        onSubmit: (event: React.FormEvent<HTMLFormElement>) => {
          event.preventDefault();
          const formData = new FormData(event.currentTarget);
          const formJson = Object.fromEntries((formData as any).entries());
          const notePath = formJson.val;
          props.dispatcher(notePath);
          props.handleClose();
        },
      }}
    >
      <DialogTitle>{props.title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{props.content}</DialogContentText>
        <TextField
          autoFocus
          required
          margin="dense"
          id="val"
          name="val"
          value={props.value}
          label={props.label}
          fullWidth
          variant="standard"
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={props.handleClose}>Cancel</Button>
        <Button type="submit">Submit</Button>
      </DialogActions>
    </Dialog>
  );
};

export default OBInputPopup;
