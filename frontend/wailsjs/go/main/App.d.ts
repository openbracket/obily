// Cynhyrchwyd y ffeil hon yn awtomatig. PEIDIWCH Â MODIWL
// This file is automatically generated. DO NOT EDIT
import {menu} from '../models';

export function DeleteFile(arg1:menu.CallbackData):Promise<void>;

export function Home():Promise<string>;

export function Move(arg1:menu.CallbackData):Promise<void>;

export function NewBook(arg1:menu.CallbackData):Promise<void>;

export function NewFile(arg1:menu.CallbackData):Promise<void>;

export function OpenFile(arg1:menu.CallbackData):Promise<void>;

export function ReadFile(arg1:string):Promise<void>;

export function RefreshTree():Promise<void>;

export function RenameFile(arg1:menu.CallbackData):Promise<void>;

export function SaveFile(arg1:menu.CallbackData):Promise<void>;

export function Version(arg1:menu.CallbackData):Promise<void>;
