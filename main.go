package main

import (
	"embed"

	"github.com/wailsapp/wails/v2"
	"github.com/wailsapp/wails/v2/pkg/menu"
	"github.com/wailsapp/wails/v2/pkg/menu/keys"
	"github.com/wailsapp/wails/v2/pkg/options"
	"github.com/wailsapp/wails/v2/pkg/options/assetserver"
	"github.com/wailsapp/wails/v2/pkg/options/linux"
	"github.com/wailsapp/wails/v2/pkg/runtime"
)

//go:embed all:frontend/dist
var assets embed.FS

func main() {
	app := NewApp()
	appMenu := createAppMenu(app)

	err := wails.Run(createAppOptions(app, appMenu))
	if err != nil {
		println("Error:", err.Error())
	}
}

func createAppMenu(app *App) *menu.Menu {
	appMenu := menu.NewMenu()
	fileMenu := appMenu.AddSubmenu("File")
	fileMenu.AddText("New Note", keys.CmdOrCtrl("n"), app.NewFile)
	fileMenu.AddText("New Book", keys.CmdOrCtrl("b"), app.NewBook)
	fileMenu.AddText("Open", keys.CmdOrCtrl("o"), app.OpenFile)
	fileMenu.AddText("Delete", keys.CmdOrCtrl("d"), app.DeleteFile)
	fileMenu.AddText("Save", keys.CmdOrCtrl("s"), app.SaveFile)
	fileMenu.AddText("Rename", keys.CmdOrCtrl("r"), app.RenameFile)
	fileMenu.AddText("Move", keys.CmdOrCtrl("m"), app.Move)
	fileMenu.AddSeparator()
	fileMenu.AddText("Quit", keys.CmdOrCtrl("q"), func(_ *menu.CallbackData) {
		runtime.Quit(app.ctx)
	})

	helpMenu := appMenu.AddSubmenu("Help")
	helpMenu.AddText("&Version", keys.CmdOrCtrl("v"), app.Version)

	return appMenu
}

func createAppOptions(app *App, appMenu *menu.Menu) *options.App {
	return &options.App{
		Title:            "obily",
		WindowStartState: options.Maximised,
		AssetServer: &assetserver.Options{
			Assets: assets,
		},
		Menu:             appMenu,
		BackgroundColour: &options.RGBA{R: 27, G: 38, B: 54, A: 1},
		OnStartup:        app.startup,
		Bind: []interface{}{
			app,
		},
		CSSDragProperty: "--wails-draggable",
		CSSDragValue:    "drag",
		Linux: &linux.Options{
			Icon:                nil,
			WindowIsTranslucent: false,
			Messages:            &linux.Messages{},
			WebviewGpuPolicy:    linux.WebviewGpuPolicyAlways,
			ProgramName:         "obily",
		},
	}
}
