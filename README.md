# obily

Create notes and store notes in a consistent manner.

## Requirements

- [OB](https://gitlab.com/openbracket/ob)
- [wails.io](https://wails.io)

## Features
    - Creates new note
    - Loads note
    - Saves note
    - Creates book
    - Delete book
    - Delete note
    - Render notes under books
    - Shortcuts - open/save/delete/new
    - Rename note
    - Rename book  
    - Nested books
    - Move note
    - Move book

## In Progress
    - Open notes via keyboard launcher

## TODO
    - Render tags as links
    - Add special Tag View
    - Render linkbacks on page

