package ob

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path"
)

type TreeDetails struct {
	Id       string
	Label    string
	IsDir    bool
	Children []TreeDetails
}

var tree []TreeDetails

func Version() string {
	cmd := exec.Command("ob", "version")
	stdout, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}

	fmt.Println(string(stdout))

	return string(stdout)
}

func Home() string {
	d, _ := os.UserHomeDir()
	return path.Join(d, "ob")
}

func NewBook(p string) string {
	cmd := exec.Command("ob", "new", "book", p)
	stdout, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}

	return string(stdout)
}

func NewNote(b string) string {
	cmd := exec.Command("ob", "new", "note", b)
	stdout, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}

	return string(stdout)
}

func MoveBook(src string, dest string) string {
	cmd := exec.Command("ob", "move", "book", src, dest)
	stdout, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}

	return string(stdout)
}

func MoveNote(src string, dest string) string {
	cmd := exec.Command("ob", "move", "note", src, dest)
	stdout, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}

	return string(stdout)
}

func DeleteBook(b string) string {
	cmd := exec.Command("ob", "delete", "book", b)
	stdout, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
		return "nooo"
	}

	return string(stdout)
}

func DeleteNote(p string) string {
	cmd := exec.Command("ob", "delete", "note", p)
	stdout, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}

	return string(stdout)
}

func ParseLinks() string {
	cmd := exec.Command("ob", "parse", "links")
	stdout, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}

	return string(stdout)
}

func ParseTags() string {
	cmd := exec.Command("ob", "parse", "tags")
	stdout, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}

	return string(stdout)
}

func WalkHomeDirectory() []TreeDetails {
	cmd := exec.Command("ob", "walk")
	stdout, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	var t []TreeDetails
	json.Unmarshal([]byte(stdout), &t)
	return t
}
