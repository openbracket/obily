package main

import (
	"context"
	"fmt"
	"obily/ob"
	"os"
	"path/filepath"

	"github.com/wailsapp/wails/v2/pkg/menu"
	"github.com/wailsapp/wails/v2/pkg/runtime"
)

type App struct {
	ctx context.Context
}

func NewApp() *App {
	return &App{}
}

func (a *App) startup(ctx context.Context) {
	a.ctx = ctx
	a.registerEventHandlers()
	a.RefreshTree()
}

func (a *App) registerEventHandlers() {
	a.registerWebLoadedHandler()
	a.registerLoadFileHandler()
	a.registerSaveFileHandler()
	a.registerNewFileHandler()
	a.registerNewBookHandler()
	a.registerDeleteBookHandler()
	a.registerDeleteNoteHandler()
	a.registerMoveBookHandler()
	a.registerMoveNoteHandler()
	a.registerRenameBookHandler()
	a.registerRenameNoteHandler()
	a.registerParseLinksHandler()
	a.registerParseTagsHandler()
	a.registerRefreshTreeHandler()
}

func (a *App) registerWebLoadedHandler() {
	runtime.EventsOn(a.ctx, "web_loaded", func(optionalData ...interface{}) {
		runtime.EventsOff(a.ctx, "web_loaded")
		runtime.EventsEmit(a.ctx, "load_tree", ob.WalkHomeDirectory())
	})
}

// registerLoadFileHandler registers an event handler for the "load_file" event.
// When the "load_file" event is emitted, this handler reads the file specified by the event data
// and emits a "read_file" event with the file's contents.
func (a *App) registerLoadFileHandler() {
	runtime.EventsOn(a.ctx, "load_file", func(optionalData ...interface{}) {
		n := ob.Home()

		for _, data := range optionalData {
			d := data.(string)

			if d != "" {
				n = filepath.Join(n, d)
			}
		}
		d, _ := os.ReadFile(n)
		runtime.EventsEmit(a.ctx, "read_file", string(d))
		a.RefreshTree()
	})
}

// registerSaveFileHandler registers an event handler for the "save_file" event.
// When the "save_file" event is emitted, this handler writes the specified data to the specified file.
func (a *App) registerSaveFileHandler() {
	runtime.EventsOn(a.ctx, "save_file", func(optionalData ...interface{}) {
		f := optionalData[0].(string)
		s := optionalData[1].(string)
		p := filepath.Join(ob.Home(), f)
		err := os.WriteFile(p, []byte(s), 0755)
		if err != nil {
			fmt.Println(err)
		}
		a.RefreshTree()
	})
}

// registerSaveFileHandler registers an event handler for the "save_file" event.
// When the "save_file" event is emitted, this handler writes the specified data to the specified file.
func (a *App) registerNewFileHandler() {
	runtime.EventsOn(a.ctx, "new_note", func(optionalData ...interface{}) {
		p := ob.NewNote(optionalData[0].(string))
		a.ReadFile(p)
		a.RefreshTree()
	})
}

func (a *App) registerNewBookHandler() {
	runtime.EventsOn(a.ctx, "new_book", func(optionalData ...interface{}) {
		ob.NewBook(optionalData[0].(string))
		a.RefreshTree()
	})
}

func (a *App) registerDeleteBookHandler() {
	runtime.EventsOn(a.ctx, "delete_book", func(optionalData ...interface{}) {
		notePath := optionalData[0].(string)
		ob.DeleteBook(notePath)
		a.RefreshTree()
	})
}

func (a *App) registerDeleteNoteHandler() {
	runtime.EventsOn(a.ctx, "delete_note", func(optionalData ...interface{}) {
		path := optionalData[0].(string)
		ob.DeleteNote(path)
		a.RefreshTree()
	})
}

func (a *App) registerMoveBookHandler() {
	runtime.EventsOn(a.ctx, "move_book", func(optionalData ...interface{}) {
		fmt.Println(optionalData)
		src := optionalData[0].(string)
		dest := optionalData[1].(string)
		ob.MoveBook(src, dest)
		a.RefreshTree()
	})
}

func (a *App) registerMoveNoteHandler() {
	runtime.EventsOn(a.ctx, "move_note", func(optionalData ...interface{}) {
		fmt.Println(optionalData)
		src := optionalData[0].(string)
		dest := optionalData[1].(string)
		ob.MoveNote(src, dest)
		a.RefreshTree()
	})
}

func (a *App) registerRenameBookHandler() {
	runtime.EventsOn(a.ctx, "rename_book", func(optionalData ...interface{}) {
		sourceBook := optionalData[0].(string)
		destBook := optionalData[1].(string)
		ob.MoveBook(sourceBook, destBook)
		a.RefreshTree()
	})
}

func (a *App) registerRenameNoteHandler() {
	runtime.EventsOn(a.ctx, "rename_note", func(optionalData ...interface{}) {
		fmt.Println(optionalData...)
		src := optionalData[0].(string)
		dest := optionalData[1].(string)
		ob.MoveNote(src, dest)
		a.RefreshTree()
	})
}

func (a *App) registerParseLinksHandler() {
	runtime.EventsOn(a.ctx, "parse_links", func(optionalData ...interface{}) {
		ob.ParseLinks()
		a.RefreshTree()
	})
}

func (a *App) registerParseTagsHandler() {
	runtime.EventsOn(a.ctx, "parse_tags", func(optionalData ...interface{}) {
		ob.ParseTags()
		a.RefreshTree()
	})
}

func (a *App) registerRefreshTreeHandler() {
	runtime.EventsOn(a.ctx, "refresh_tree", func(optionalData ...interface{}) {
		a.RefreshTree()
	})
}

// Version prints the version of the application.
func (a *App) Version(_ *menu.CallbackData) {
	fmt.Println(ob.Version())
}

func (a *App) NewFile(_ *menu.CallbackData) {
	runtime.EventsEmit(a.ctx, "new_file")
}

func (a *App) NewBook(_ *menu.CallbackData) {
	runtime.EventsEmit(a.ctx, "create_book")
}

func (a *App) OpenFile(_ *menu.CallbackData) {
	selection, _ := runtime.OpenFileDialog(a.ctx, runtime.OpenDialogOptions{
		Title: "Select File",
		Filters: []runtime.FileFilter{
			{
				DisplayName: "Markdown (*.md;*.mdx)",
				Pattern:     "*.md;*.mdx",
			},
		},
	})

	d, _ := os.ReadFile(selection)
	runtime.EventsEmit(a.ctx, "read_file", string(d))
}

func (a *App) DeleteFile(_ *menu.CallbackData) {
	runtime.EventsEmit(a.ctx, "delete_file")
}

func (a *App) SaveFile(_ *menu.CallbackData) {
	runtime.EventsEmit(a.ctx, "save_note")
}

func (a *App) RenameFile(_ *menu.CallbackData) {
	runtime.EventsEmit(a.ctx, "rename_file")
}

func (a *App) Move(_ *menu.CallbackData) {
	runtime.EventsEmit(a.ctx, "move")
}

// Home returns the home directory of the application.
func (a *App) Home() string {
	return ob.Home()
}

func (a *App) RefreshTree() {
	fmt.Println(ob.WalkHomeDirectory())
	runtime.EventsEmit(a.ctx, "load_tree", ob.WalkHomeDirectory())
}

func (a *App) ReadFile(p string) {
	d, _ := os.ReadFile(p)
	runtime.EventsEmit(a.ctx, "read_file", string(d))
}
